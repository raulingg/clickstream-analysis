const getServerConfig = () => ({
  port: process.env.PORT || 5000
})
export default getServerConfig